# MobileBDD

#### 看一下项目结构及其每个package的作用
![输入图片说明](image/1.png)

#### 软件架构
com.itproject.bdd.mobile.framework.actions：封装了AOS和IOS的元素操作，比如判断元素是否出现，点击元素，输入input框，获取屏幕宽高等。

com.itproject.bdd.mobile.framework.config：读取配置文件/环境变量/系统属性

com.itproject.bdd.mobile.framework.context：测试环境上下文，当前测试的是IOS还是AOS，已经运行完成的feature和正在运行的feature，当前正在执行的Scenario

com.itproject.bdd.mobile.framework.driver：启动/退出AOS或IOS的driver

com.itproject.bdd.mobile.framework.hooks: Cucumber hook, 设置执行feature前应该做什么，执行每一个step前后应该处理什么，执行完每一个Scenario后应该做什么。

com.itproject.bdd.mobile.testcase.pages: App界面封装，由于AOS和IOS的元素操作有比较大的区别，所以定义了一个Screen接口，AOS和IOS有不同的实现。

com.itproject.bdd.mobile.testcase.steps：step定义，通用的step定义到BaseStep


#### 测试用例
![输入图片说明](image/2.png)

#### 使用说明
直接运行RunMobileTest

pom.xml目录中运行mvn test

